import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.bc.issue.search.SearchService.ParseResult;
import com.atlassian.jira.event.type.EventDispatchOption;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchResults;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.query.Query;
import com.atlassian.jira.jql.builder.JqlClauseBuilder;
import com.atlassian.jira.jql.builder.JqlQueryBuilder;
import com.atlassian.jira.issue.search.SearchProvider;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.customfields.option.Option;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.fields.Field;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.search.managers.SearchHandlerManager;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.jql.operand.JqlOperandResolver;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.web.bean.PagerFilter;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.issue.search.SearchProvider;
import com.atlassian.jira.bc.issue.IssueService;
import com.atlassian.jira.jql.parser.JqlQueryParser;
import java.lang.Math;
import java.util.*;
//SORT BY VALUE:
//FUNCTION OF SORTING OUT HASHMAPS BY THEIR VALUES:
public LinkedHashMap<Double, String> sortHashMapByValues(HashMap<Double, String> passedMap) {
    List<Double> mapKeys = new ArrayList<>(passedMap.keySet());
    List<String> mapValues = new ArrayList<>(passedMap.values());
    Collections.sort(mapValues);
    Collections.sort(mapKeys);
    LinkedHashMap<Double, String> sortedMap =new LinkedHashMap<>();
    Iterator<String> valueIt = mapValues.iterator();
    while (valueIt.hasNext()) {
        String val = valueIt.next();
        Iterator<Double> keyIt = mapKeys.iterator();
        while (keyIt.hasNext()) {
            Double key = keyIt.next();
            String comp1 = passedMap.get(key);
            String comp2 = val;
            if (comp1 == comp2) {
                keyIt.remove();
                sortedMap.put(key, val);
                break;
            }
        }
    }
    return sortedMap;
}
//DECLARATION:
final FieldManager fieldManager=ComponentAccessor.getFieldManager();
final SearchService searchService=ComponentAccessor.getComponent(SearchService.class);
final IssueService issueService=ComponentAccessor.getIssueService();
final User=ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser();
//List<Issue> Issues=ComponentAccessor.getIssueManager().
//GET CUSTOM FIELDS LIST (SYSTEM NOT INCLUDED):
public List<CustomField> getCustomFields() {
    List<CustomField> RESULT=ComponentAccessor.getCustomFieldManager().getCustomFieldObjects();
    for (int i=0; i<RESULT.size(); i++) {
        if (RESULT[i].getCustomFieldType().getName()=="Text Field (single line)") {
            return RESULT[i];
        }
    }
}
//FUNCTION OF SIMILARITY BETWEEN TWO STRINGS:
public static double similarity(String s1, String s2) {
    String longer = s1, shorter = s2;
    if (s1.size() < s2.size()) {
        longer = s2; shorter = s1;
    }
    int longerLength = longer.size();
    if (longerLength == 0) {
        return 1.0; /* both strings are zero length */
    }

    return ((longerLength - editDistance(longer, shorter)) / (double) longerLength)*100;

}

public static int editDistance(String s1, String s2) {
    s1 = s1.toLowerCase();
    s2 = s2.toLowerCase();
    int[] costs = new int[s2.length() + 1];
    for (int i = 0; i <= s1.length(); i++) {
        int lastValue = i;
        for (int j = 0; j <= s2.length(); j++) {
            if (i == 0)
                costs[j] = j;
            else {
                if (j > 0) {
                    int newValue = costs[j - 1];
                    if (s1.charAt(i - 1) != s2.charAt(j - 1))
                        newValue = Math.min(Math.min(newValue, lastValue),
                            costs[j]) + 1;
                    costs[j - 1] = lastValue;
                    lastValue = newValue;
                }
            }

        }
        if (i > 0)
            costs[s2.length()] = lastValue;
    }
    return costs[s2.length()];
}

//GET ISSUES WITH SIMILAR NAME TO INPUT SUMMARY:
public List<Issue> getSimilarIssues(String name){
    def projectManager = ComponentAccessor.getProjectManager();
    def User = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser();
    def issueManager = ComponentAccessor.getIssueManager();
    def customFieldManager = ComponentAccessor.getCustomFieldManager();
//def jqlQueryParser = ComponentAccessor.getComponent(JqlQueryParser);
    def searchProvider = ComponentAccessor.getComponent(SearchProvider);
    List<Project> Projects=projectManager.getProjectObjects();
    def
        issueMgr = ComponentAccessor.getIssueManager();
    List<Issue> IssuesAvailable=[];
    for (int j=0;j<Projects.size();j++) {
        def issues = issueMgr.getIssueObjects(issueMgr.getIssueIdsForProject(Projects.get(j).getId()));
        //log.error("list of issues is  "+ issues);
        for (int k=0; k<issues.size();k++){
            IssuesAvailable.add(issues[k]);
        }

    }
    def SearchService searchService=ComponentAccessor.getComponent(SearchService.class);
    SearchService.ParseResult parseResult =  searchService.parseQuery(User, " summary ~ \"*"+name+"*\"" );
    def searchResult = searchService.search(User, parseResult.getQuery(), PagerFilter.getUnlimitedFilter());
    List issues = searchResult.getResults();
    List Summaries=new ArrayList();
    for (int z=0; z<issues.size(); z++) {
        Summaries.add(issues.get(z).getSummary());
    }
    Summaries.sort();
    int n=Summaries.size();
    double max=0;
    HashMap<Double,String> map = new HashMap<Double,String>();
    for (int t=0;t<Summaries.size();t++) {
        map.put(similarity(name,(String)Summaries.get(t)),(String)Summaries.get(t));
    }
    sortHashMapByValues(map);
    Map<Double, String> newMap = new TreeMap<>(Collections.reverseOrder());
    newMap.putAll(map);
    return newMap;

}
getSimilarIssues("t")

